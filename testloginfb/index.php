<?php 
    require_once 'init.php';

    if(isset($_SESSION['fb_access_token'])){
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,email,birthday,picture.type(large)', $_SESSION['fb_access_token']);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
          
        $user = $response->getGraphUser();
        var_dump($user);
        foreach($user as $key=>$value){
            if($key == 'picture'){
                $imgUrl = base64_encode(file_get_contents($value['url']));
                echo "<img src='data:image/png;base64,".$imgUrl."' >";
            }else if($key == 'birthday'){
                echo $value->date.'<br>';
            }else{
                echo $value."<br>";
            }
        }
        session_destroy();
    }else{
        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email,user_birthday']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('http://localhost/testloginfb/fb-callback.php', $permissions);

        echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
    }




    
